#ansible-cifs-mounts

This repository contains an ansible playbook in order to mount cifs directories, recording them in fstab

Usage:


```
#!yaml

hosts: my.host.example.com
  roles:
  - dledanseur.cifs-mounts
  vars:
  cifs_mounts:
  - { src: "/path/on/host" dest: "//server.name/volume.name" user: "credential_user_name" password: "credential_password" }

```
